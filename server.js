const express = require('express');
const bodyParser = require('body-parser'); ///바디파서 불러오기(설치는 npm install body-parser)
const morgan = require('morgan');
const passport = require('passport')
const dotEnv = require('dotenv')
dotEnv.config()

const app = express();

const productRoutes = require('./router/product');
const orderRoutes = require('./router/order');
const userRoutes = require('./router/user')
const profileRoutes = require('./router/profile')

//////database연결
require('./config/database')



////middleware
app.use(morgan("dev"));
app.use(bodyParser.json()); ////이건 post와 put&patch 전용 함수 body태그의 입력값을 받기위해쓴다
app.use(bodyParser.urlencoded({ extended: false})); ///사용 구문이다
app.use(passport.initialize())

require('./config/passport')(passport)

//routing
app.use('/product', productRoutes);
app.use('/order', orderRoutes);
app.use('/user', userRoutes)
app.use('/profile', profileRoutes)

const port = process.env.PORT
app.listen(port, console.log(`server started at ${port}`))
