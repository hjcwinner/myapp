const mongoose = require('mongoose')
const orderModel = mongoose.Schema({
    productid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'productio',
        required : true
    },
    quantity : {
        type : Number,
        required : true
    }
})

module.exports = mongoose.model('order', orderModel)