const mongoose = require('mongoose')


const db_Options  = {
    useUnifiedTopology: true, 
    useNewUrlParser: true,
    useFindAndModify : false,
    useCreateIndex : true
}

mongoose
    .connect(process.env.MONGODB_URI, db_Options)
    .then(() => console.log("database연결완료..."))
    .catch(err => console.log(err.message));