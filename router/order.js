const express = require('express')
const { orders_get_detail, orders_patch_order, orders_register_order, orders_delete_order, orders_get_all } = require('../controllers/order')
const router = express.Router()

//////전체보기
router.get('/', orders_get_all)

//////1개보기
router.get('/:orderid', orders_get_detail)

//////등록하기
router.post('/', orders_register_order)

//////수정하기
router.patch('/:orderid', orders_patch_order)

//////삭제하기
router.delete('/:orderid', orders_delete_order)

module.exports = router