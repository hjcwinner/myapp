const express = require('express')
const router = express.Router()
const passport = require('passport')


const userModel = require('../model/user')
const { user_register, user_get, user_login } = require('../controllers/user')

const checkAuth = passport.authenticate('jwt', { session: false })

router.get('/', user_get)
/////register
router.post('/register', user_register)
/////login
router.post('/login', user_login)


// @ route    http://localhost:8080/user/current
// @ desc     get current user from jwt
// @ acess    Private
router.get('/current', checkAuth, (req, res) => {
    // res.status(200).json({
    //     id : req.user.id,
    //     userid : req.user.userid,
    //     email : req.user.email,
    // })
    userModel
        .findById(req.user.id)
        .then(user => {
            res.status(200).json(user)
        })
        .catch(err => {
            res.status(500).json({
                message : err.message
            })
        })
})




module.exports = router