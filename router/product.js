const express = require("express")
const { products_get_all, products_get_detail, products_patch_product, products_delete_product, products_register_product } = require("../controllers/product")
const router = express.Router()



// product 전체 불러오기
router.get("/",  products_get_all)


// product 한개 불러오기
router.get("/:produc",  products_get_detail)


// product 등록하기
router.post("/",  products_register_product)


// product 수정하기
router.patch("/:productid",  products_patch_product)


// product 삭제하기
router.delete("/:ddelete",  products_delete_product)

module.exports = router

