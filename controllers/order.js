const orderModel = require('../model/order')


exports.orders_get_all = (req, res) => {
    orderModel
        .find()
        .populate('productid',["name", "price"])
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        product : doc.productid,
                        quantity : doc.quantity,
                        request : {
                            type : "get",
                            url : "http://localhost:8080/order/" + doc._id
                        }
                    }
                })
            }
            res.json(response)
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_get_detail = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findById(id)
        .populate('productid',["name", "price"])
        .then(doc => {
            res.json({
                message : id + "이상품 주문",
                product : {
                    id : doc._id,
                    product : doc.productid,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:8080/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_register_order = (req, res) => {
    const {product, qty} = req.body
    const order = new orderModel({
        productid : product,
        quantity : qty
    })

    order
        .save()
        .then(doc => {
            res.json({
                message : "save",
                orderInfo : {
                    id : doc._id,
                    product : doc.productid,
                    qauntity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:8080/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_patch_order = (req, res) => {
    const id = req.params.orderid
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.aaaaa] = ops.bbbbb
    }

    orderModel
        .findByIdAndUpdate(id, { $set: updateOps})
        .then(result => {
            res.json({
               message : "updata complete",
               request : {
                    type : "get",
                    url : "http://localhost:8080/order"
               }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_delete_order = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findByIdAndDelete(id)
        .then(() => {
            res.json({
                message : "delete",
                request : {
                    type : "get",
                    url : "http://localhost:8080/order"
                }
            })
        })
}