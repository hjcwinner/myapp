const jwt = require('jsonwebtoken')
const userModel = require('../model/user')

function tokenGenertor(payload)
{
    return jwt.sign(
        payload,
        process.env.SECRET_KEY,
        { expiresIn : '1d'}
    )
}


exports.user_register = (req, res) => {
    ////id & email형식체크 및 중복=> password 암호화 => 회원가입
    const {userid, email, password} = req.body
     userModel
         .findOne({email})
         .then(user => {
             if(user) {
                 return res.json({
                     message : "중복된 이메일입니다."
                 })
             }
             else 
             {                  
                    const newUser = new userModel({
                        userid, email, password,
                    })
                    
                    newUser
                        .save()
                        .then(doc => {
                            res.json({
                                message : "save",
                                userInfo : {
                                    id : doc._id,
                                    userid : doc.userid,
                                    email : doc.email,
                                    password : doc.password,
                                    avatar : doc.avatar
                                }
                            })
                        })
                        .catch(err => {
                            res.json({
                                message : err.message
                            })
                        })            
             }
         })
         .catch(err => {
             res.json({
                 message : err.message
             })
         })
    }

exports.user_login = (req, res) =>{
    ////email(id)중복체크 => password일치확인 => login
    const {email, password} = req.body
    userModel
      .findOne({email})
      .then(user => {
          if(!user) {
           return res.json({
                  message : "가입된 이메일이 없습니다."
              })
          }
          else 
          {
                user.comparePassword(password, (err, isMatch) => {
                    if(err || isMatch === false)
                    {
                        return res.json({
                            message : "password incorrect"
                        })
                    }
                    else
                    {
                        const payload = {id : user._id, userid : user.userid, email : user.email, avatar : user.avatar}
                        res.json({
                            successful : isMatch,
                            tokenInfo : tokenGenertor(payload)
                        })  
                    }
                })
            }
      })
      .catch(err => {
          res.json({
              message : err.message
          })
      })
  }

  exports.user_get = (req, res) => {
    userModel
        .find()
        .then(docs => {
            res.json({
                count : docs.length,
                userInfo : docs.map(doc => {
                    return{
                        id : doc._id,
                        userid : doc.userid,
                        email : doc.email,
                        password : doc.password
                    }
                })
            })
    
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

