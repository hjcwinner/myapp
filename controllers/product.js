const productModel = require('../model/product')


exports.products_get_all = (req, res) => {
    productModel
        .find()
        .then(docs => {
            const response = {
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:8080/product/" + doc._id
                        }
                    }
                })            
            }
            res.json(response)   
        })    
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_get_detail = (req, res) => {
    const id = req.params.produc
    productModel
        .findById(id)
        .then(doc => {
            if(!doc)
            {
                res.json({
                message : "제품 없음"  
                })             
            }
            else
            {
                res.json({
                    product : {
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:8080/product"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_register_product = (req, res) => {
    const {productname, productprice} = req.body
    const product = new productModel({
        name : productname,   ////여기키값은 model에 product.js에 schema키값이랑 같아야함
        price : productprice ////여기키값은 model에 product.js에 schema키값이랑 같아야함
    });


    product
        .save()
        .then(doc => {
            res.json({
                product : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : {
                        type : "post",
                        url : "http://localhost:8080/product/" + doc._id
                    }
                }
            })
        })
        .catch(err => {
        res.json({
            error : err.message
        })
    });
}

exports.products_patch_product = (req, res) => {
    const id = req.params.productid
    const updateOps = {}
    for(const ops of req.body) {
        updateOps[ops.aaaa] = ops.bbbb
    }
        
    productModel
        .findByIdAndUpdate(id, { $set: updateOps})
        .then(result => {
            res.json({
                product : {
                    id : result._id,
                    name : result.name,
                    price : result.price,
                    request : {
                        type : "update",
                        url : "http://localhost:8080/product"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_delete_product = (req, res) => {
    const id = req.params.ddelete
    productModel
        .findByIdAndDelete(id)
        .then(docdel => {
            if(!docdel)
            {
                res.json({
                    message : "선택된상품없음"
                })  
            }
            else
            {
                res.json({
                    message : "삭제완료",
                    request : {
                        type : "GET",
                        url : "http://localhost:8080/product"
                    }

                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}